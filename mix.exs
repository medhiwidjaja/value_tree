defmodule ValueTree.MixProject do
  use Mix.Project

  def project do
    [
      app: :value_tree,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:nary_tree, path: "/Users/medhi/Dev/elixirs/nary_tree"}
    ]
  end
end
