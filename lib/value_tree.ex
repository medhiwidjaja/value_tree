defmodule ValueTree do
  alias NaryTree
  alias NaryTree.Node

  @moduledoc """
  Documentation for ValueTree.
  """

  def new do
    new_node("Goal", %{w: 1.0})
    |> NaryTree.new()
  end

  def new_node(name, %{} = content) do
    Node.new(name, content: content)
  end
  
  @doc """
  Normalize.

  ## Examples

      iex> ValueTree.normalize(tree)
      :world

  """
  def normalize(tree) do
    %Node{NaryTree.root(tree) | content: %{normal: true}}
  end
end
